/*
 * Program to implement an associative key-value data structure - LinkedHashTable
 * 
 * Interface: Table
 * Class: TableFactory, MainClass
 * Exception: NonExistingKey
 * @author - Jeremy Brown
 * 
 * Class: LinkedHashTable, Pair
 * @author - Jaydeep Untwal
 * 
 * Class: TestTable
 * @author    - Russell Uhl
 * @co-author - Jaydeep Untwal
 * 
 * Rochester Institute of Technology
 */

using System;
using System.Collections.Generic;

namespace RIT_CS
{

    /// <summary>
    /// A Class to test implemented Hash Table
    /// </summary>
    public class TestTable
    {

        public static bool result = true;

        /// <summary>
        /// Method to test various test cases
        /// </summary>
        public static void test()
        {

            // Test 1
            // Different Keys, Different Values, No Rehash
            Table<String, int> table0 = TableFactory.Make<String, int>(10, 0.5);
            for (int i = 0; i < 3; i++)
            {
                table0.Put("" + i, i);
            }
            for (int i = 0; i < 3; i++)
            {
                if(!(table0.Get("" + i).Equals(i))){
                    Console.WriteLine("Test 1 Failed");
                    result = false;
                    break;
                }
            }

            // Test 2
            // Different Keys, Same Value, Rehash boundary
            Table<String, int> table1 = TableFactory.Make<String, int>(10, 0.5);
            for (int i = 0; i < 5; i++)
            {
                table1.Put("" + i, 1);
            }
            for (int i = 0; i < 5; i++)
            {
                if (!(table1.Get("" + i).Equals(1)))
                {
                    Console.WriteLine("Test 2 Failed");
                    result = false;
                    break;
                }
            }

            // Test 3
            // Different Keys, Same Value, Rehash mandatory
            Table<String, int> table2 = TableFactory.Make<String, int>(10, 0.5);
            for (int i = 0; i < 10; i++)
            {
                table2.Put("" + i, 1);
            }
            for (int i = 0; i < 10; i++)
            {
                if (!(table2.Get("" + i).Equals(1)))
                {
                    Console.WriteLine("Test 3 Failed");
                    result = false;
                    break;
                }
            }

            // Test 4
            // Testing ThreshLevel fringe case.  Results may vary.  VALUE = 1.
            Table<String, int> table3 = TableFactory.Make<String, int>(10, 0.99);
            for (int i = 0; i < 10; i++)
            {
                table3.Put("" + i, 1);
            }
            for (int i = 0; i < 10; i++)
            {
                if (!(table3.Get("" + i).Equals(1)))
                {
                    Console.WriteLine("Test 4 Failed");
                    result = false;
                    break;
                }
            }

            // Test 5
            // Testing ThreshLevel > 1.  Should NOT rehash.  VALUE = 1.
            Table<String, int> table4 = TableFactory.Make<String, int>(10, 2.5);
            for (int i = 0; i < 15; i++)
            {
                table4.Put("" + i, 1);
            }
            for (int i = 0; i < 15; i++)
            {
                if (!(table4.Get("" + i).Equals(1)))
                {
                    Console.WriteLine("Test 5 Failed");
                    result = false;
                    break;
                }
            }

            // Test 6
            // Testing duplicate KEYs.  Final VALUE should = 9.
            Table<String, int> table5 = TableFactory.Make<String, int>(10, 0.5);
            for (int i = 0; i < 10; i++)
            {
                table5.Put("1", i);
            }

            if (!(table5.Get("1").Equals(9)))
            {
                Console.WriteLine("Test 6 Failed");
                result = false;
            }

            // Test 7
            // Testing default arguments to CTOR. Multiple VALUEs.
            Table<String, int> table6 = TableFactory.Make<String, int>();
            for (int i = 0; i < 10; i++)
            {
                table6.Put("" + i, i);
            }
            for (int i = 0; i < 10; i++)
            {
                if (!(table6.Get("" + i).Equals(i)))
                {
                    Console.WriteLine("Test 7 Failed");
                    result = false;
                    break;
                }
            }

            // Test 8
            // Testing algorithm time with 1M entries.  Should take 8 seconds or less.
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            Table<String, int> table7 = TableFactory.Make<String, int>(10, 0.5);
            for (int i = 0; i < 1000000; i++)
            {
                table7.Put("" + i, i);
            }
            for (int i = 0; i < 1000000; i++)
            {
                if (!table7.Contains("" + i))
                {
                    Console.WriteLine("Test 8 Failed");
                    result = false;
                    break;
                }
            }
            sw.Stop();
            Console.WriteLine("Time taken to Put and check Contains for 1M (different key - different value) Pairs\n"
                + sw.Elapsed.Seconds + " sec");

        }
    }


    /// <summary>
    /// A class to store key-value pair
    /// </summary>
    /// <typeparam name="K">Type of key</typeparam>
    /// <typeparam name="V">Type of Value</typeparam>
    class Pair<K, V>
    {

        private K key;
        private V value;

        public Pair(K key, V value)
        {
            this.key = key;
            this.value = value;
        }

        public V getValue()
        {
            return this.value;
        }

        public K getKey()
        {
            return this.key;
        }

        public void setValue(V value)
        {
            this.value = value;
        }

    }

   
    /// <summary>
    /// A Data structure implemeting the Table interface
    /// </summary>
    /// <typeparam name="K">Type of key</typeparam>
    /// <typeparam name="V">Type of value</typeparam>
    class LinkedHashTable<K, V> : Table<K, V>
    {

        /// <summary>
        /// An array of Lists which store Key-Value pair
        /// </summary>
        private List<Pair<K, V>>[] table;

        /// <summary>
        /// Total Capacity of the Hash Table
        /// </summary>
        private int capacity;

        /// <summary>
        /// Percentage at which the table is rehashed
        /// </summary>
        private double loadFactor;

        /// <summary>
        /// Current size of the Hash Table
        /// (No. of Pairs)
        /// </summary>
        private int size;


        /// <summary>
        /// Default Hash Table
        /// </summary>
        public LinkedHashTable()
        {
            capacity = 16;
            loadFactor = 0.5;
            size = 0;
            table = new List<Pair<K, V>>[capacity];
        }

        /// <summary>
        /// Hash Table with specified capacity
        /// </summary>
        /// <param name="capacity">Capacity of the table</param>
        public LinkedHashTable(int capacity)
        {
            this.capacity = capacity;
            loadFactor = 0.5;
            size = 0;
            table = new List<Pair<K, V>>[capacity];
        }


        /// <summary>
        /// Hash Table with specified capacity and loadFactor
        /// </summary>
        /// <param name="capacity">Capacity of the table</param>
        /// <param name="loadFactor">Percentage at which it should be rehashed</param>
        public LinkedHashTable(int capacity, double loadFactor)
        {
            this.capacity = capacity;
            this.loadFactor = loadFactor;
            size = 0;
            table = new List<Pair<K, V>>[capacity];
        }

        
        /// <summary>
        /// Method to increase the capacity of the table
        /// and rehash all existing nodes
        /// </summary>
        private void rehash()
        {
            size = 0;
            int newCapacity = capacity * 2;
            List<Pair<K, V>>[] newTable = new List<Pair<K, V>>[newCapacity];

            // Add all existing Pairs to new table
            for (int i = 0; i < table.Length; i++)
            {
                if (table[i] != null)
                {
                    foreach (Pair<K, V> temp in table[i])
                    {
                        rePut(temp.getKey(), temp.getValue(), newTable, newCapacity);
                    }
                }
            }

            capacity = newCapacity;
            table = newTable;

        }

        /// <summary>
        /// Method to readd all existing nodes into new table
        /// </summary>
        /// <param name="key">Key to be added</param>
        /// <param name="value">Value of Pair</param>
        /// <param name="table">New Table</param>
        /// <param name="capacity">New Capacity</param>
        private void rePut(K key, V value, List<Pair<K, V>>[] table, int capacity)
        {
            Pair<K, V> pair = new Pair<K, V>(key, value);
            int pos = Math.Abs(key.GetHashCode() % capacity);

            // Make new list if not initialized
            if (table[pos] == null)
            {
                table[pos] = new List<Pair<K, V>>();
                table[pos].Add(pair);
                size++;
                return;
            }

            // Check if key already exists
            foreach (Pair<K, V> temp in table[pos])
            {
                if (temp.getKey().Equals(pair.getKey()))
                {
                    temp.setValue(pair.getValue());
                    return;
                }
            }

            // Add new Pair
            table[pos].Add(pair);
            size++;
        }

        /// <summary>
        /// To add a new entry in the hash table
        /// Position of the key is obtained by its hash code
        /// </summary>
        /// <param name="key">Key to be added</param>
        /// <param name="value">Value to be added</param>
        public void Put(K key, V value)
        {
            Pair<K, V> pair = new Pair<K, V>(key, value);
            int pos = Math.Abs(key.GetHashCode() % capacity);

            // Add first Pair
            if (table[pos] == null)
            {
                table[pos] = new List<Pair<K, V>>();
                table[pos].Add(pair);
                size++;
                return;
            }

            // Update value if key exists
            foreach (Pair<K, V> temp in table[pos])
            {
                if (temp.getKey().Equals(pair.getKey()))
                {
                    temp.setValue(pair.getValue());
                    return;
                }
            }

            // Add new Pair
            table[pos].Add(pair);
            size++;

            // Check if table is to be rehased
            if (size / capacity > loadFactor)
            {
                rehash();
            }
        }

        /// <summary>
        /// Check whether a given key exists in the table
        /// </summary>
        /// <param name="key">Key to be searched</param>
        /// <returns>True iff key is found</returns>
        public bool Contains(K key)
        {
            int pos = Math.Abs(key.GetHashCode() % capacity);

            // Check if list exists
            if (table[pos] == null)
            {
                return false;
            }

            // Check if key exists in list
            foreach (Pair<K, V> temp in table[pos])
            {
                if (temp.getKey().Equals(key))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// To obtain a value associated with the key
        /// </summary>
        /// <param name="key">Key to be searched</param>
        /// <returns>Value associated with that key</returns>
        public V Get(K key)
        {
            int pos = Math.Abs(key.GetHashCode() % capacity);

            // Check if list exists
            if (table[pos] == null)
            {
                throw new NonExistentKey<K>(key);
            }

            // Check if key exists in list
            foreach (Pair<K, V> temp in table[pos])
            {
                if (temp.getKey().Equals(key))
                {
                    return temp.getValue();
                }
            }

            throw new NonExistentKey<K>(key);
        }

        /// <summary>
        /// To Enumerate the table and return each key
        /// </summary>
        /// <returns>One key at a time</returns>
        public IEnumerator<K> GetEnumerator()
        {
            // Traverse each list
            for (int i = 0; i < table.Length; i++)
            {
                if(table[i] != null){

                    // Return One key and resume using yeild
                    foreach (Pair<K, V> temp in table[i])
                    {
                        yield return temp.getKey();
                    }
                }
            }
        }

        /// <summary>
        /// To obtain an enumerator to the table
        /// </summary>
        /// <returns>Enumerator to the table</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }



    /// <summary>
    /// An exception used to indicate a problem with how
    /// a HashTable instance is being accessed
    /// </summary>
    public class NonExistentKey<Key> : Exception
    {
        /// <summary>
        /// The key that caused this exception to be raised
        /// </summary>
        public Key BadKey { get; private set; }

        /// <summary>
        /// Create a new instance and save the key that
        /// caused the problem.
        /// </summary>
        /// <param name="k">
        /// The key that was not found in the hash table
        /// </param>
        public NonExistentKey(Key k) :
            base("Non existent key in HashTable: " + k)
        {
            BadKey = k;
        }

    }

    /// <summary>
    /// An associative (key-value) data structure.
    /// A given key may not appear more than once in the table,
    /// but multiple keys may have the same value associated with them.
    /// Tables are assumed to be of limited size are expected to automatically
    /// expand if too many entries are put in them.
    /// </summary>
    /// <param name="Key">the types of the table's keys (uses Equals())</param>
    /// <param name="Value">the types of the table's values</param>
    interface Table<Key, Value> : IEnumerable<Key>
    {
        /// <summary>
        /// Add a new entry in the hash table. If an entry with the
        /// given key already exists, it is replaced without error.
        /// put() always succeeds.
        /// (Details left to implementing classes.)
        /// </summary>
        /// <param name="k">the key for the new or existing entry</param>
        /// <param name="v">the (new) value for the key</param>
        void Put(Key k, Value v);

        /// <summary>
        /// Does an entry with the given key exist?
        /// </summary>
        /// <param name="k">the key being sought</param>
        /// <returns>true iff the key exists in the table</returns>
        bool Contains(Key k);

        /// <summary>
        /// Fetch the value associated with the given key.
        /// </summary>
        /// <param name="k">The key to be looked up in the table</param>
        /// <returns>the value associated with the given key</returns>
        /// <exception cref="NonExistentKey">if Contains(key) is false</exception>
        Value Get(Key k);
    }

    class TableFactory
    {
        /// <summary>
        /// Create a Table.
        /// (The student is to put a line of code in this method corresponding to
        /// the name of the Table implementor s/he has designed.)
        /// </summary>
        /// <param name="K">the key type</param>
        /// <param name="V">the value type</param>
        /// <param name="capacity">The initial maximum size of the table</param>
        /// <param name="loadThreshold">
        /// The fraction of the table's capacity that when
        /// reached will cause a rebuild of the table to a 50% larger size
        /// </param>
        /// <returns>A new instance of Table</returns>
        public static Table<K, V> Make<K, V>(int capacity = 100, double loadThreshold = 0.75)
        {
            return new LinkedHashTable<K,V>(capacity,loadThreshold);
        }
    }

    /// <summary>
    /// Main class to create and test Linked Hash Table
    /// </summary>
    class MainClass
    {
        public static void Main(string[] args)
        {

            Table<String, String> ht = TableFactory.Make<String, String>(4, 0.5);
            ht.Put("Joe", "Doe");
            ht.Put("Jane", "Brain");
            ht.Put("Chris", "Swiss");
            
            try
            {
                foreach (String first in ht)
                {
                    Console.WriteLine(first + " -> " + ht.Get(first));
                }
                Console.WriteLine("=========================");

                ht.Put("Wavy", "Gravy");
                ht.Put("Chris", "Bliss");
                foreach (String first in ht)
                {
                    Console.WriteLine(first + " -> " + ht.Get(first));
                }
                Console.WriteLine("=========================");

                Console.Write("Jane -> ");
                Console.WriteLine(ht.Get("Jane"));
                Console.Write("John -> ");
                Console.WriteLine(ht.Get("John"));
            }
            catch (NonExistentKey<String> nek)
            {
                Console.WriteLine(nek.Message);
                Console.WriteLine(nek.StackTrace);
            }


            // Test using own test class

            Console.WriteLine("");
            Console.WriteLine("Please wait! Now testing 8 Test cases on HashTable using Test Class");

            TestTable.test();

            Console.WriteLine("Tests Completed");

            if (TestTable.result)
            {
                Console.WriteLine("All Tests passed successfully!");
            }

            Console.ReadLine();
        }
    }
}